$(document).ready(function() {
	$('.order-btn').magnificPopup({
		type: 'inline'
	});

	$('.popup-form, .inline-form').submit(function(e) {

        var name = $(this).find('.name').val();
        var phone = $(this).find('.phone').val();
        var email = $(this).find('.email').val();
        var comment = $(this).find('.comment').val();

        $.post('send.php', {
            name: name,
            phone: phone,
            email: email,
            comment: comment
        },
        function(data) {
            if (data === 'sended') {
                $.magnificPopup.open({
                	items: {
                		src: '#thanks',
                		type: 'inline'
                	}
                });
            } else {
                alert('Сообщение не отправлено');
            }
        });

        return false;

    });
});